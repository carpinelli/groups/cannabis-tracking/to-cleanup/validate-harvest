# Validate-Harvest

This project is no longer maintained here. An updated version can be found as a script [here](https://www.gitlab.com/jcarpinelli/medvs/).
This project uses a Python script that uses OpenPyXL to parse and validate harvest data stored in an '.xlsx' file at work. The validated data is then used to generate a report and '.csv' files intended for management and METRC.

## Contact

mr.carpinelli@protonmail.ch

## Building

The code files provided are written as scripts. The scripts use Python3, the OpenPyXl module, Microsoft batch files (meant to work on at least Windows 7), and git for CMD.

## Contributing

This is mostly a pretty specific personal project, but any contributions are welcome and appreciated. Forks and clones are also welcome, provided the [LICENSE](LICENSE) file is kept.

## License

All contributions are made under the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html). See the [LICENSE](LICENSE).

